import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Ferma {
	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";

	 public static String now() {
	    Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
	    return sdf.format(cal.getTime());

	  }

 public static void main(String[] args){
   BigInteger value=new BigInteger("22845935");
   BigInteger step=new BigInteger("1");
   System.out.println(now()+": Program start\r\n");
   long i=0;
   while (true){
	 value=value.add(step);
	 boolean cond1=CheckNumber(value,3);
	 boolean cond2=CheckNumber(value,4);
     if (cond1||cond2){
       //System.out.println("value="+value.toString()+"\r\n");	 
	   BreakNumber(value);
     }
     i++;
     if (i>100000){
    	 System.out.println(now()+": Steel alive :) value="+value.toString()+"\r\n");
    	 i=0;
     }
   }
 }
 
 private static boolean CheckNumber(BigInteger num,int base){
	 //int scale=6;
	 BigDecimal val=new BigDecimal(num.toString());
	 BigDecimal result=takeRoot(base,val,new BigDecimal("0.0000001"));
	/* BigDecimal result=exp(ln(val,scale).multiply(new BigDecimal((1/base))), scale);
	 result=result.multiply(new BigDecimal("1000000"));
	 result=result.add(new BigDecimal("1"));
	 result=result.divide(new BigDecimal("1000000"));*/
	 BigDecimal bg[] = result.divideAndRemainder(new BigDecimal("1"));
	 /*System.out.println("Result="+result.toPlainString()+"\r\n");
	 System.out.println("base="+base+"\r\n");
	 System.out.println("val="+val.toString()+"\r\n");
	 System.out.println("Reminder="+bg[1].toPlainString()+" compare to="+bg[1].compareTo(new BigDecimal("0"))+"\r\n");*/
	 if (bg[1].compareTo(new BigDecimal("0.0000001"))<0){
 	  return true;
	 }
	 else
	  return false;	 
 }
 
  
 private static void BreakNumber(BigInteger num){

	 BigInteger v1=new BigInteger("2");
	 BigInteger v2=num;
	 BigInteger cond=new BigInteger("1");
	 BigInteger check=num.divide(new BigInteger("2"));
	 check=check.subtract(new BigInteger("1"));			 
	 //System.out.println("source num="+num.toString());
	 //System.out.println("num="+check.toString());
	 while (v2.compareTo(check)>0){
		 v1=v1.add(new BigInteger("1"));
		 v2=num;
		 v2=v2.subtract(v1);
		 boolean cond1=CheckNumber(v1,3);
		 boolean cond2=CheckNumber(v1,4);
		 boolean cond3=CheckNumber(v2,3);
		 boolean cond4=CheckNumber(v2,4);
		/* if (cond1||cond2||cond3||cond4){
		  System.out.println("num="+num.toString()+"\r\n");
		  System.out.println("v1="+v1.toString()+" v2="+v2.toString()+"\r\n");		  		  
		  System.out.println("cond1="+cond1+" cond2="+cond2+" cond3="+cond3+" cond4="+cond4+"\r\n");
		 }*/
	      if (cond1||cond2)
	       if (cond3||cond4){
	    	System.out.println("*********Start********************\r\n");
	    	if (v1.gcd(v2).compareTo(cond)==0&&v1.gcd(num).compareTo(cond)==0&&v2.gcd(num).compareTo(cond)==0){
		     System.out.println("Unbelivable :) \r\n");  
	    	} else {
	         System.out.println("Fucking shiet ;( \r\n");
	         System.out.println("v1.gcd(v2)="+v1.gcd(v2).toString()+"\r\n");
	         System.out.println("v1.gcd(num)="+v1.gcd(num).toString()+"\r\n");
	         System.out.println("v2.gcd(num)="+v2.gcd(num).toString()+"\r\n");
	    	}
	    	System.out.println("Bingo:v1="+v1.toString()+"\r\n");	    		
		    System.out.println("v2="+v2.toString()+"\r\n");
		    System.out.println("num="+num.toString()+"\r\n");
		    System.out.println("*********End********************\r\n");
		  }
		 
	   }
 }

 /**
  * Compute the natural logarithm of x to a given scale, x > 0.
  */
 public static BigDecimal ln(BigDecimal x, int scale)
 {
     // Check that x > 0.
     if (x.signum() <= 0) {
         throw new IllegalArgumentException("x <= 0");
     }

     // The number of digits to the left of the decimal point.
     int magnitude = x.toString().length() - x.scale() - 1;

     if (magnitude < 3) {
         return lnNewton(x, scale);
     }

     // Compute magnitude*ln(x^(1/magnitude)).
     else {

         // x^(1/magnitude)
         BigDecimal root = intRoot(x, magnitude, scale);

         // ln(x^(1/magnitude))
         BigDecimal lnRoot = lnNewton(root, scale);

         // magnitude*ln(x^(1/magnitude))
         return BigDecimal.valueOf(magnitude).multiply(lnRoot)
                     .setScale(scale, BigDecimal.ROUND_HALF_EVEN);
     }
 }

 /**
  * Compute the natural logarithm of x to a given scale, x > 0.
  * Use Newton's algorithm.
  */
 private static BigDecimal lnNewton(BigDecimal x, int scale)
 {
     int        sp1 = scale + 1;
     BigDecimal n   = x;
     BigDecimal term;

     // Convergence tolerance = 5*(10^-(scale+1))
     BigDecimal tolerance = BigDecimal.valueOf(5)
                                         .movePointLeft(sp1);

     // Loop until the approximations converge
     // (two successive approximations are within the tolerance).
     do {

         // e^x
         BigDecimal eToX = exp(x, sp1);

         // (e^x - n)/e^x
         term = eToX.subtract(n)
                     .divide(eToX, sp1, BigDecimal.ROUND_DOWN);

         // x - (e^x - n)/e^x
         x = x.subtract(term);

         Thread.yield();
     } while (term.compareTo(tolerance) > 0);

     return x.setScale(scale, BigDecimal.ROUND_HALF_EVEN);
 }
 
 /**
  * Compute the integral root of x to a given scale, x >= 0.
  * Use Newton's algorithm.
  * @param x the value of x
  * @param index the integral root value
  * @param scale the desired scale of the result
  * @return the result value
  */
 public static BigDecimal intRoot(BigDecimal x, long index,
                                  int scale)
 {
     // Check that x >= 0.
     if (x.signum() < 0) {
         throw new IllegalArgumentException("x < 0");
     }

     int        sp1 = scale + 1;
     BigDecimal n   = x;
     BigDecimal i   = BigDecimal.valueOf(index);
     BigDecimal im1 = BigDecimal.valueOf(index-1);
     BigDecimal tolerance = BigDecimal.valueOf(5)
                                         .movePointLeft(sp1);
     BigDecimal xPrev;

     // The initial approximation is x/index.
     x = x.divide(i, scale, BigDecimal.ROUND_HALF_EVEN);

     // Loop until the approximations converge
     // (two successive approximations are equal after rounding).
     do {
         // x^(index-1)
         BigDecimal xToIm1 = intPower(x, index-1, sp1);

         // x^index
         BigDecimal xToI =
                 x.multiply(xToIm1)
                     .setScale(sp1, BigDecimal.ROUND_HALF_EVEN);

         // n + (index-1)*(x^index)
         BigDecimal numerator =
                 n.add(im1.multiply(xToI))
                     .setScale(sp1, BigDecimal.ROUND_HALF_EVEN);

         // (index*(x^(index-1))
         BigDecimal denominator =
                 i.multiply(xToIm1)
                     .setScale(sp1, BigDecimal.ROUND_HALF_EVEN);

         // x = (n + (index-1)*(x^index)) / (index*(x^(index-1)))
         xPrev = x;
         x = numerator
                 .divide(denominator, sp1, BigDecimal.ROUND_DOWN);

         Thread.yield();
     } while (x.subtract(xPrev).abs().compareTo(tolerance) > 0);

     return x;
 }
 
 /**
  * Compute e^x to a given scale.
  * Break x into its whole and fraction parts and
  * compute (e^(1 + fraction/whole))^whole using Taylor's formula.
  * @param x the value of x
  * @param scale the desired scale of the result
  * @return the result value
  */
 public static BigDecimal exp(BigDecimal x, int scale)
 {
     // e^0 = 1
     if (x.signum() == 0) {
         return BigDecimal.valueOf(1);
     }

     // If x is negative, return 1/(e^-x).
     else if (x.signum() == -1) {
         return BigDecimal.valueOf(1)
                     .divide(exp(x.negate(), scale), scale,
                             BigDecimal.ROUND_HALF_EVEN);
     }

     // Compute the whole part of x.
     BigDecimal xWhole = x.setScale(0, BigDecimal.ROUND_DOWN);

     // If there isn't a whole part, compute and return e^x.
     if (xWhole.signum() == 0) return expTaylor(x, scale);

     // Compute the fraction part of x.
     BigDecimal xFraction = x.subtract(xWhole);

     // z = 1 + fraction/whole
     BigDecimal z = BigDecimal.valueOf(1)
                         .add(xFraction.divide(
                                 xWhole, scale,
                                 BigDecimal.ROUND_HALF_EVEN));

     // t = e^z
     BigDecimal t = expTaylor(z, scale);

     BigDecimal maxLong = BigDecimal.valueOf(Long.MAX_VALUE);
     BigDecimal result  = BigDecimal.valueOf(1);

     // Compute and return t^whole using intPower().
     // If whole > Long.MAX_VALUE, then first compute products
     // of e^Long.MAX_VALUE.
     while (xWhole.compareTo(maxLong) >= 0) {
         result = result.multiply(
                             intPower(t, Long.MAX_VALUE, scale))
                     .setScale(scale, BigDecimal.ROUND_HALF_EVEN);
         xWhole = xWhole.subtract(maxLong);

         Thread.yield();
     }
     return result.multiply(intPower(t, xWhole.longValue(), scale))
                     .setScale(scale, BigDecimal.ROUND_HALF_EVEN);
 }
 
 /**
  * Compute e^x to a given scale by the Taylor series.
  * @param x the value of x
  * @param scale the desired scale of the result
  * @return the result value
  */
 private static BigDecimal expTaylor(BigDecimal x, int scale)
 {
     BigDecimal factorial = BigDecimal.valueOf(1);
     BigDecimal xPower    = x;
     BigDecimal sumPrev;

     // 1 + x
     BigDecimal sum  = x.add(BigDecimal.valueOf(1));

     // Loop until the sums converge
     // (two successive sums are equal after rounding).
     int i = 2;
     do {
         // x^i
         xPower = xPower.multiply(x)
                     .setScale(scale, BigDecimal.ROUND_HALF_EVEN);

         // i!
         factorial = factorial.multiply(BigDecimal.valueOf(i));

         // x^i/i!
         BigDecimal term = xPower
                             .divide(factorial, scale,
                                     BigDecimal.ROUND_HALF_EVEN);

         // sum = sum + x^i/i!
         sumPrev = sum;
         sum = sum.add(term);

         ++i;
         Thread.yield();
     } while (sum.compareTo(sumPrev) != 0);

     return sum;
 }
 
 /**
  * Compute x^exponent to a given scale.  Uses the same
  * algorithm as class numbercruncher.mathutils.IntPower.
  * @param x the value x
  * @param exponent the exponent value
  * @param scale the desired scale of the result
  * @return the result value
  */
 public static BigDecimal intPower(BigDecimal x, long exponent,
                                   int scale)
 {
     // If the exponent is negative, compute 1/(x^-exponent).
     if (exponent < 0) {
         return BigDecimal.valueOf(1)
                     .divide(intPower(x, -exponent, scale), scale,
                             BigDecimal.ROUND_HALF_EVEN);
     }

     BigDecimal power = BigDecimal.valueOf(1);

     // Loop to compute value^exponent.
     while (exponent > 0) {

         // Is the rightmost bit a 1?
         if ((exponent & 1) == 1) {
             power = power.multiply(x)
                       .setScale(scale, BigDecimal.ROUND_HALF_EVEN);
         }

         // Square x and shift exponent 1 bit to the right.
         x = x.multiply(x)
                 .setScale(scale, BigDecimal.ROUND_HALF_EVEN);
         exponent >>= 1;

         Thread.yield();
     }

     return power;
 }
 
 public static BigDecimal takeRoot(int root, BigDecimal n, BigDecimal maxError) {
     int MAXITER = 5000;

     // Specify a math context with 40 digits of precision.
     MathContext mc = new MathContext(40);

     // Specify the starting value in the search for the cube root.
     BigDecimal x;
     x=new BigDecimal("1",mc);

     
     BigDecimal prevX = null;
    
     BigDecimal rootBD = new BigDecimal(root,mc);
     // Search for the cube root via the Newton-Raphson loop. Output each successive iteration's value.
     for(int i=0; i < MAXITER; ++i) {
         x = x.subtract(x.pow(root,mc)
                .subtract(n,mc)
                .divide(rootBD.multiply(x.pow(root-1,mc),mc),mc),mc);
         if(prevX!=null && prevX.subtract(x).abs().compareTo(maxError) < 0)
             break;
         prevX = x;
     }
    
     return x;
 }

}
